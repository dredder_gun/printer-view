
(ns flexview.auth.core
  (:require
    [mlib.log :refer [debug info warn]]
    [mlib.http :refer [json-resp]]
    ;
    [flexview.auth.bcrypt :refer [check-pass]]
    [flexview.auth.sess :refer [new-sid sess-save]]
    [flexview.auth.user :refer [user-by-login]]))
;


(defn do-login [{params :params :as req}]
  (let [login     (-> params :login str)
        password  (-> params :password str)
        user      (user-by-login login)]
    ;
    (if (and user (check-pass password (:passhash user)))
      (let [sid (new-sid)]
        (sess-save sid (-> user :id))
        (json-resp {:ok 1 :authtok sid}))
      ;;
      (json-resp
        {:err "user" :msg "Неверный логин или пароль."}))))
;

;;.

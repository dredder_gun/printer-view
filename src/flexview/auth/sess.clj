
(ns flexview.auth.sess
  (:require
    [mlib.log :refer [debug info warn]]
    [mlib.core :refer [urand-bytes hexbyte]]

    [flexview.auth.user :refer [user-by-id]]))
;


(def RE_BEARER #"(?i)Bearer\\s+(.+)")

;; Authorization: Bearer authtok
(defn wrap-authtok [handler]
  (fn [req]
    (let [auth (get-in req [:headers "authorization"])]
      ; (prn "headers:" (:headers req))
      (if-let [authtok (second (re-matches RE_BEARER (str auth)))]
        (handler (assoc req :authtok authtok))
        (handler req)))))
;


(defn new-sid []
  (apply str
    (format "%x" (System/currentTimeMillis)) "."
    (map hexbyte (urand-bytes 10))))
;


;; {authtok user_id}
(defonce sess-map (atom {}))


(defn sess-load [key]
  (get @sess-map key))
;

(defn sess-save [key val]
  (swap! sess-map assoc key val))
;


;;; ;;; ;;; ;;; ;;;

(defn wrap-user [handler]
  (fn [req]
    (let [sess (sess-load (:authtok req))]
      (if-let [user sess]   ;; NOTE: sess/user/id
        (handler (assoc req :user user))
        (handler req)))))
;

;;.

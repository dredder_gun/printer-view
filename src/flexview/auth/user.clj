
(ns flexview.auth.user
  (:require
    [mount.core :refer [defstate]]
    ;
    [mlib.log :refer [debug info warn]]
    [mlib.conf :refer [conf]]))
;


(defstate user-db
  :start
    (-> conf :user-db))
;

(defn user-by-id [id]
  (->> user-db
    (some #(and (= id (:id %)) %))))
;

(defn user-by-login [login]
  (->> user-db
    (some #(and (= login (:login %)) %))))
;


;;.

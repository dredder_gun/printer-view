
(ns flexview.app.printer
  (:require
    [clojure.java.io :as io]
    [mlib.http :refer [json-resp]]
    [clj-http.client :as client]
    [clojure.data.json :as json]))

(defn to-print
  [{:keys [params]}]
  (prn "to printer" {:device {:id "posiflexAura"}
                :check {:header {:orderNumber "7_970"
                                 :posNumber 1
                                 :checkNumber 77000972
                                 :dateTime (:date params)
                                 :waiter "Сергей"
                                 :table 3.5}
                        :rows (into [] (map (fn [item] (:clauses (second item))) (:data params)))}})
  (let [checks {:device {:id "posiflexAura"}
                :check {:header {:orderNumber "7_970"
                                 :posNumber 1
                                 :checkNumber 77000972
                                 :dateTime (:date params)
                                 :waiter "Сергей"
                                 :table 3.5}
                        :rows (into [] (map (fn [item] (:clauses (second item))) (:data params)))}}]
    (client/post "http://localhost:8080/api/print"
      {:body
        (json/write-str checks) :content-type :json})
    (json-resp {:ok checks})))


(ns flexview.app.routes
  (:require
    [clojure.java.io :as io]
    [mlib.log :refer [debug info warn]]
    [ring.util.response :refer [redirect not-found]]
    [compojure.core :refer [GET POST ANY context routes]]
    [compojure.route :as route]

    ; [monger.collection :as mc]
    ; [monger.operators :refer [$set $unset]]

    [mlib.conf :refer [conf]]
    [mlib.http :refer [json-resp text-resp]]
    [mlib.time :refer [now-ms]]
    [mlib.web.sess :refer [wrap-sess sid-resp]]
    [mlib.web.middleware :refer [middleware]]
    ;
    [flexview.auth.core :refer [do-login]]
    [flexview.auth.sess :refer [wrap-authtok wrap-user]]
    [clj-http.client :as client]
    [flexview.app.printer :refer [to-print]]))
;

; (defn wrap-user-required [handler]
;   (fn [req]
;     (if (:user req)
;       (handler req)
;       (redirect
;         (str (-> conf :urls :login) "?redir=" (:uri req))))))
; ;


(defn api-404 [req]
  { :status  404
    :headers {"Content-Type" "text/plain"}
    :body    "API endpoint not found"})
;


(defn make-routes []
  (routes
    (GET  "/"   _
        (fn [_]
          { :status 200
            :headers {"Content-Type" "text/html;charset=utf-8"}
            :body (io/input-stream (io/resource "public/app/index.html"))}))

    (GET  "/auth/login" [] do-login)

    (POST "/auth/login" [] do-login)

    (GET "/api/checklist" [] (:body (client/get "http://localhost:8080/api/checklist" {:accept :json})))

    (POST "/send/print" [] to-print)

    ;(context "/api/"     _ nil)


    (if (:dev conf)
      (route/files "/" {:root "tmp/devroot"})
      (route/resources "/" {:root "public"}))

    (ANY "/*" _  (fn [_] (not-found "Not Found")))))
;


; (defn wrap-user [handler]
;   (fn [req]
;     (if-let [uid (-> req :sess :uid str not-empty)]
;       (handler (assoc req :user
;                   (user-by-id uid FLDS_REQ_USER)))
;       (handler req))))
; ;
;
;
; (defn wrap-slowreq [handler cnf]
;   (let [ms (:ms cnf)]
;     (fn [req]
;       (let [t0 (do (now-ms))
;             resp (do (handler (assoc req :start-time t0)))
;             dt (do (- (now-ms) t0))]
;         (when (< ms dt)
;           (info "slowreq:" dt (:remote-addr req)(:uri req)))
;         resp))))
; ;

(defn app-routes []
  (->
    (make-routes)
    (wrap-user)
    (wrap-authtok)
;    (wrap-user)
    ;; (wrap-csrf {:skip-uris #{"/auth/login"}})   ;; TODO: fix
;    (wrap-sess sess-load)
    middleware))
;    (wrap-slowreq (:slowreq conf))))

;;.


(ns flexview.app.srv
  (:require
    [mlib.log :refer [debug info warn]]
    [ring.adapter.jetty :refer [run-jetty]]
    [mount.core :refer [defstate]]
    ;
    [mlib.conf :refer [conf]]
    [flexview.app.routes :refer [app-routes]]))
;

(defn start [handler]
  (let [hc (:http conf)]
    (info "build -" (:build conf))
    (info "start server -" hc)
    (run-jetty handler hc)))
;

(defstate server
  :start
    (start (app-routes))
  :stop
    (.stop server))
;

;;.

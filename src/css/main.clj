(ns css.main
  (:refer-clojure :exclude [>])
  (:require
    [garden.def :refer [defstyles]]
    [garden.units :refer [px pt em ex]]
    [garden.selectors :refer [>]]
    [garden.stylesheet :refer [at-media]]))

(def main-styles
  [:#app-root
    [:#main-header
      {:text-align "center"
      :font-weight "bold"
      :color "grey"
      :margin "30px auto"}]
    [:#check-container
      {:display "flex"
       :flex-direction "column"}]
    [:.add-item-container
      {:display "flex"
       :justify-content "space-around"}]
    [:.check-item-container
      {:display "flex"
       :justify-content "space-around"}]
    [:.button-bottom
      {:display "flex"
        :justify-content "end"}]])

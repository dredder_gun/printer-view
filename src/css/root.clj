
(ns css.root
  (:refer-clojure :exclude [>])
  (:require
    [garden.def :refer [defstyles]]
    [garden.units :refer [px pt em ex]]
    [garden.selectors :refer [>]]
    [garden.stylesheet :refer [at-media]]
    ;
    [css.frame :refer [frame-styles main-pane]]
    [css.main :refer [main-styles]]))
;

(def p100 "100%")

(def fa "#fafafa")
(def f8 "#f8f8f8")
(def f4 "#f4f4f4")
(def bd "#bdbdbd")
(def ccc "#ccc")
(def ddd "#ddd")

(def gcurr "#6af")

(def hdr-brd "#039be5")

(def c_a          "#18d")
(def c_av         "#18e")
(def c_ah         "#09f")
(def c_aa         "#a80")
(def c_em         "#E65100")
(def c_dlg_title  "#3949AB")

(def c_topbar_brd "#039BE5")      ; light blue 600


;;; ;;; ;;; ;;;


(def abs-pos
  {:position :absolute :top 0 :left 0 :bottom 0 :right 0})
;

(defn brd1 [place color]
  {(str "border-" (name place)) (str "1px solid " color)})
;


(def commons
  [
    ; [:body
    ;   { :font-family
    ;       "\"Open Sans\",\"Helvetica Neue\",Helvetica,Arial,sans-serif"
    ;     :font-size (px 15)}]

    [:a
      {:text-decoration 'none :color "#18d"}]
    [:a:visited
      {:text-decoration 'none :color "#18e"}]
    [:a:hover
      {:text-decoration 'underline :color "#09f"}]
    [:a:active
      {:text-decoration 'underline :color "#a80"}]

    [:p.txt {:text-indent (em 2)}]

    [:.flex-mid
      { :display 'flex
        :flex-direction 'column
        :justify-content 'center}]
    [:.flex-bot
      { :display 'flex
        :flex-direction 'column
        :justify-content 'flex-end}]

    [:.msg
      { :color "#888"
        :padding (px 2)}]
    [:.msg_ok
      { :color "#090"
        :font-weight "bold"}]
    [:.msg_err
      { :color "#a00"
        :font-weight "bold"}]

    [:.wh100 {:width "100vw" :height "100vh"}]])
;

(def margins
  [
    [:.amar {:margin-left 'auto :margin-right 'auto}]

    [:.marl-4 {:margin-left "4px"}]
    [:.marl-6 {:margin-left "6px"}]
    [:.marl-8 {:margin-left "8px"}]

    [:.marr-4 {:margin-right "4px"}]
    [:.marr-6 {:margin-right "6px"}]
    [:.marr-8 {:margin-right "8px"}]])

;

(def botnav
  [:.b-botnav {:text-align 'center :margin "16px auto 8px auto"}])
;

(def footer
  [:.b-footer
    (merge
      (brd1 :top c_topbar_brd)
      { :margin-top (px 8)
        :padding "1px 0 0 0"})

    [:.footer-bg
      { :padding "1.3ex 2.4ex 1.4ex 2.4ex"
        :background-color "#f4f4f4"
        :border-bottom-left-radius "4px"
        :border-bottom-right-radius "4px"}
      [:.copy
        {:color "#777"}
        [:.brand {:margin-left "2 px" :margin-right "1px"}]]]])
;


(def content
  [:.content
    [:.page-title
      { :margin-top "8px"
        :letter-spacing "0.12ex"
        :text-align 'center
        :color "#48c"}]])
;

(def content
  [:.head-table
    {:font-weight "bold"}])
;

(defstyles main
  commons
  margins
  main-styles

  frame-styles
  main-pane

  content

  botnav
  footer)
;

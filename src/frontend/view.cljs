(ns frontend.view
  (:require
    [reagent.core :as r]
    [re-frame.core :as rf]
    [frontend.modal :refer [modal hello-bootstrap]]
    [frontend.events]
    [frontend.subs]))

(defn checks-list
  []
  (let [checks @(rf/subscribe [:checks])
        time-id (fn [p] (:dateTime (:header (:check p))))]
    [:div.col-md-3
      [:button {:type "button"
                :on-click #(rf/dispatch [:show-layout-add])} "Добавить"]
      [:ul
        (for [check checks]
          ^{:key (time-id check)} [:li {:on-click #(rf/dispatch [:show-layout-check check])} (time-id check)])]]))

(defn clauses-to-new-check
  []
  (let [check @(rf/subscribe [:check-to-add])]
    (if (empty? check)
      nil
      (into
          [:tbody]
          (map
            (fn [item]
               [:tr
                  [:td [:input#select.select-item {:type "checkbox"
                                                   :checked (:selected item)
                                                   :on-change #(rf/dispatch [:toggle-to-select (:id item)])}]]
                  [:td [:p (:guest item)]]
                  [:td [:p (:goodName item)]]
                  [:td [:p (:count item)]]
                  [:td [:button "-"]]])
                 check)))))

(defn add-form
  []
  (let [data-to-add (r/atom {})]
    (fn []
      [:tr
        [:td ""]
        [:td [:input#name.form-control {:type "text"
                                        :form "add-new"
                                        :value (:guest @data-to-add)
                                        :on-change #(swap! data-to-add assoc :guest (-> % .-target .-value))}]]
        [:td [:input#goodName.form-control {:type "text"
                                         :form "add-new"
                                         :value (:goodName @data-to-add)
                                         :on-change #(swap! data-to-add assoc :goodName (-> % .-target .-value))}]]
        [:td [:input#count.form-control {:type "text"
                                         :form "add-new"
                                         :value (:count @data-to-add)
                                         :on-change #(swap! data-to-add assoc :count (-> % .-target .-value))}]]
        [:td [:button {:type "submit"
                       :form "add-new"
                       :on-click #(do
                                    (.preventDefault %)
                                    (rf/dispatch [:add-clause-new-check @data-to-add])
                                    (reset! data-to-add {}))}
                        "+"]]])))

(defn check-view
  []
  (let [selected-check @(rf/subscribe [:selected-check])]
    [:div#check-container.row
      [:form#add-new.add-item-container
        [:table.table
          [:thead.head-table
            [:tr
              [:td "Гость"]
              [:td "Наименование"]
              [:td "Количество"]]]
          selected-check]]
      [:div.form-group.button-bottom
        {:style {:margin-top "20px"}}]]))

(defn add-view
  []
  [:div#check-container.row
    [:form#add-new.add-item-container
      [:table.table
        [:thead.head-table
          [:tr
            [:td "Выбрать"]
            [:td "Гость"]
            [:td "Наименование"]
            [:td "Количество"]
            [:td ""]]]
        [clauses-to-new-check]
        [add-form]]]
    [:div.form-group.button-bottom
      {:style {:margin-top "20px"}}
      [:button.btn.btn-default
        { :type "submit"
          :on-click #(do
                      (.preventDefault %)
                      (rf/dispatch [:remove-from-add]))}
         [:b "Удалить"]]
      [:button.btn.btn-success
        { :type "submit"
          :on-click #(do
                      (.preventDefault %)
                      (rf/dispatch [:modal
                                      {:show? true
                                       :child [hello-bootstrap]
                                       :size :small}]))}
         [:b "Печать"]]]])

(defn new-check-block
  []
  (condp = @(rf/subscribe [:layout])
    ;
    :check    [check-view]
    ;
    :add      [add-view]
    ;
    ;; error!
    [:div.panel.panel-default
            [:div.panel-body "Выберите чек или добавьте новый"]]))

(defn view
  []
  [:div.container
    [:h1#main-header "Принтер"]
    [modal]
    [checks-list]
    [:div.col-md-8.col-md-offset-1
      [new-check-block]
      (let [error @(rf/subscribe [:errors])]
        (fn []
          [:div.text-center
            (when-let [msg (-> error :msg not-empty)]
              [:div.panel.panel-danger [:div.panel-heading "Ошибка" [:h4.panel-title msg]]])]))]])

(ns frontend.events
  (:require
    [frontend.db :refer [default-value]]
    [re-frame.core :refer [reg-event-db reg-event-fx inject-cofx path trim-v
                           after debug dispatch reg-fx reg-cofx trim-v]]
    [ajax.core :as ajax]
    [frontend.addings.http-fx]
    [frontend.addings.print_ln :refer [prt]]))

(def CHECKS "/api/checklist")
(def PRINT "/send/print")

(reg-event-db
  :get-checks-success
  []
  (fn [db [_ res]]
    (prn "Успешно" (assoc default-value :checks (js->clj res)))
    (if (not-empty res)
          (assoc default-value :checks (js->clj res))
          default-value)))

(reg-event-db
  :get-checks-fail
  []
  (fn [db _]
    (prn "Fail!")
    (assoc default-value :errors {:msg "Чеки с сервера не загрузились"})))

(reg-event-fx
  :initialise-db
  []
  (fn [cofx _]
    (assoc cofx :http-xhrio {:method          :get
                              :uri             CHECKS
                              :format          (ajax/json-request-format)
                              :response-format (ajax/json-response-format {:keywords? true})
                              :on-success      [:get-checks-success]
                              :on-failure      [:get-checks-fail]})))

(reg-event-db
 :modal
 (fn [db [_ data]]
   (assoc-in db [:modal] data)))

(reg-event-db
  :show-layout-add
  [trim-v]
  (fn [db _]
    (assoc db :layout :add)))

(reg-event-db
  :show-layout-check
  [trim-v]
  (fn [db [check]]
    (assoc db :layout :check :selected-check check)))

(reg-event-db
  :add-clause-new-check
  [trim-v]
  (fn [db [param]]
    (let [next-id (count (:check-to-add db))]
      (if (not (empty? param))
        (assoc-in db [:check-to-add next-id] (assoc param :id next-id :selected false))
        (do
          (prn "Параметры не заполнены")
          db)))))

(comment [{:guest "Fsdff", :count "sdgfg", :goodName "34"}] "Это в :check-to-add")

(reg-event-db
  :success-print
  [trim-v]
  (fn [db [ok]]
    (prn "ok" ok)
    (update-in (assoc db :check-to-add []) [:checks] conj (:ok ok))))

(reg-event-fx
  :send-to-printer
  []
  (fn [cofx _]
    (let [check (atom {})
          counter (atom 0)
          clauses_vector (:check-to-add (:db cofx))
          clauses_map (map #(do
                        (swap! counter inc)
                        (swap! check assoc @counter {:id @counter :clauses %})) clauses_vector)]
    (dorun clauses_map)
    (prn "@check" @check)
    (assoc cofx :http-xhrio {:method          :post
                              :uri             PRINT
                              :format          (ajax/json-request-format)
                              :response-format (ajax/json-response-format {:keywords? true})
                              :on-success      [:success-print]
                              :on-failure      [:fail-print]
                              :params {:data @check :date (.toISOString (js/Date.))}}))))

(reg-event-db
  :hide-check
  []
  (fn [db _]
    (assoc db :show-check nil)))

(reg-event-db
  :show-one-check
  [trim-v]
  (fn [db [check]]
    (prn ":show-check" (assoc db :show-check check))
    (assoc db :show-check check)))

(reg-event-db
  :toggle-to-select
  [trim-v]
  (fn [db [id]]
    (prn "toggle-to-select" (if (some #(= % id) (:selected-id-clauses db))
                                  (update-in db [:selected-id-clauses] conj id)
                                  (update-in db [:selected-id-clauses] remove #(= % id))))
    (-> (if (some #(= % id) (:selected-id-clauses db))
          (update-in db [:selected-id-clauses] conj id)
          (update-in db [:selected-id-clauses] remove #(= % id)))
      (update-in [:check-to-add id :selected] not))))

(reg-event-db
  :remove-from-add
  [trim-v]
  (fn [db _]
    (let [id-to-delete (:selected-id-clauses db)]
      (assoc
        (->> (apply dissoc (:check-to-add db) id-to-delete)
          (assoc-in db [:check-to-add]))
        :selected-id-clauses
        []))))

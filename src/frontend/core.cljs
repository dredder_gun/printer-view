(ns frontend.core
  (:require
    [reagent.core :as r]
    [re-frame.core :as rf]
    [frontend.view :refer [view]]))
;
(enable-console-print!)
;
(defn ^:export run []
  (.log js/console "run.")
  (rf/dispatch-sync [:initialise-db])
  (r/render
    view
    (js/document.getElementById "app-root")))
;;.

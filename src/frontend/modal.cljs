(ns frontend.modal
  (:require
   [re-frame.core :refer [dispatch subscribe]]))


(defn modal-panel
  [{:keys [child size show?]}]
  [:div {:class "modal-wrapper"}
   [:div {:class "modal-backdrop"
          :on-click (fn [event]
                      (do
                        (dispatch [:modal {:show? (not show?)
                                           :child nil
                                           :size :default}])
                        (.preventDefault event)
                        (.stopPropagation event)))}]
   [:div {:class "modal-child"
          :style {:width (case size
                           :extra-small "15%"
                           :small "30%"
                           :large "70%"
                           :extra-large "85%"
                           "50%")}} child]])

(defn modal []
 (let [modal (subscribe [:modal])]
   (fn []
     [:div
      (if (:show? @modal)
        [modal-panel @modal])])))


(defn- close-modal []
  (dispatch [:modal {:show? false :child nil}]))


(defn- hello []
  [:div
   {:style {:background-color "white"
            :padding          "16px"
            :border-radius    "6px"
            :text-align "center"}} "Hello modal!"])


(defn- hello-bootstrap []
  [:div {:class "modal-content panel-danger"}
   [:div {:class "modal-header panel-heading"}

    [:h4 {:class "modal-title"} "Распечатка чека"]]
   [:div {:class "modal-body"}
    [:div [:b (str "Отправить чек на печать?")]]]
   [:div {:class "modal-footer"}
    [:button {:type "button" :title "Нет"
              :class "btn btn-default"
              :on-click #(close-modal)} "Нет"]
    [:button {:type "button" :title "Да"
              :class "btn btn-default"
              :on-click #(do
                          (dispatch [:send-to-printer])
                          (dispatch [:not-add-check])
                          (close-modal))} "Ok"]]])

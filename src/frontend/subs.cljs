(ns frontend.subs
  (:require [re-frame.core :refer [subscribe reg-sub-raw reg-sub]]
    [reagent.ratom :as ratom :include-macros true]
    [frontend.addings.print_ln :refer [prt]]
    [frontend.addings.print_ln])
  (:require-macros [reagent.ratom :refer [reaction]]))

(reg-sub-raw
 :modal
 (fn [db _] (reaction (:modal @db))))

(reg-sub
 :checks
 (fn [db _]
   (:checks db)))

(reg-sub
  :layout
  (fn [db _]
    (:layout db)))

(reg-sub
  :checked-elements-vector
  (fn [db _]
    (into [] (map #(second %) (:check-to-add db)))))

(reg-sub
  :check-to-add
  :<- [:checked-elements-vector]
  (fn [checked-elements-vector _]
    (prn "checked-elements-vector" checked-elements-vector)
    checked-elements-vector))

(reg-sub
  :errors
  (fn [db _]
    (prt "error" db)
    (:errors db)))

(reg-sub
  :selected-check
  (fn [db _]
    (prn ":selected-check" (:rows (:check (:selected-check db))))
    (into
        [:tbody]
        (map
          (fn [item]
             [:tr
                [:td [:p (:guest item)]]
                [:td [:p (:count item)]]
                [:td [:p (:goodName item)]]])
               (:rows (:check (:selected-check db)))))))

(reg-sub
  :selected-clauses
  (fn [db _]
    (prn "selected" (:selected-clauses db))
    (:selected-clauses db)))

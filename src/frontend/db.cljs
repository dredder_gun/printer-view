(ns frontend.db
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame]
            [cljs.reader]
            [frontend.addings.http-fx]))


(def default-value
  {:checks  (sorted-map)
   :check-to-add {}
   :selected-id-clauses []
   :modal {:show? false :child nil :size :large}
   :layout nil
   :errors nil
   :selected-check nil
   :show-check nil})
